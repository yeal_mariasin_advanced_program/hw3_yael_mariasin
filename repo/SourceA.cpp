#include "sqlite3.h"
#include <iostream>
#include <string>
#include <sstream> //std::stringstream
using namespace std;

//Global variable ID
int id = -1;


//Returns last inserted row ID
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int result = 0;
	
	for (int i = 0; i < argc; i++)
	{
		result = result * 10 + (*argv[i] - '0');
	}

	id = result;

	return 0;
}

int check_error(int rc)
{
	char *zErrMsg = 0;

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	return 0;
}

int main()
{
	int rc;
	sqlite3* db;
	std::stringstream s;
	char *zErrMsg = 0;
	
	rc = sqlite3_open("FirstPart.db", &db); // connection to the database
	if (check_error(rc))
		return 1;
	


	rc = sqlite3_exec(db, "create table people(id integer primary key autoincrement, name string);", NULL, 0, &zErrMsg);
	if (check_error(rc))
		return 1;

	rc = sqlite3_exec(db, "insert into people(name) values(\"Yeal\");", NULL, 0, &zErrMsg);
	if (check_error(rc))
		return 1;

	rc = sqlite3_exec(db, "insert into people(name) values(\"Lior\");", NULL, 0, &zErrMsg);
	if (check_error(rc))
		return 1;

	rc = sqlite3_exec(db, "insert into people(name) values(\"Daniel1\");", NULL, 0, &zErrMsg);
	if (check_error(rc))
		return 1;
	
	
	rc = sqlite3_exec(db, "SELECT last_insert_rowid() FROM people", callback, &id, &zErrMsg);
	if (check_error(rc))
		return 1;

	char sql_str[60];
	sprintf_s(sql_str, "update people set name = Igal where id=%d", id);
	rc = sqlite3_exec(db, sql_str, NULL, 0, &zErrMsg);
	if (check_error(rc))
		return 1;

	sqlite3_close(db);

	system("PAUSE");
	return 0;
}
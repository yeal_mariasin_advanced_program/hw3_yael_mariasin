#include "sqlite3.h" 
#include <iostream>
#include <string>
#include <sstream> //std::stringstream
using namespace std;

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc;
	int balanceFrom = 0, balanceTo = 0;

	rc = sqlite3_exec(db, "begin transaction", callback, NULL, &zErrMsg);
	if (check_error(rc))
		return false;

	char sql_str1[60];
	sprintf_s(sql_str1, "SELECT balance FROM accounts where id=%d", from);
	rc = sqlite3_exec(db, sql_str1, callback, &balanceFrom, &zErrMsg);
	if (check_error(rc))
		return false;

	char sql_str2[60];
	sprintf_s(sql_str2, "SELECT balance FROM accounts where id=%d", to);
	rc = sqlite3_exec(db, sql_str2, callback, &balanceTo, &zErrMsg);
	if (check_error(rc))
		return false;

	if (balanceFrom < amount)
	{
		return false;
	}

	char sql_str3[100];
	sprintf_s(sql_str3, "update accounts set balance = %d where id=%d", (balanceFrom - amount), from);
	rc = sqlite3_exec(db, sql_str3, NULL, 0, &zErrMsg);
	if (check_error(rc))
		return false;

	char sql_str4[100];
	sprintf_s(sql_str4, "update accounts set balance = %d where id=%d", (balanceTo + amount), to);
	rc = sqlite3_exec(db, sql_str4, NULL, 0, &zErrMsg);
	if (check_error(rc))
		return false;

	rc = sqlite3_exec(db, "commit", callback, NULL, &zErrMsg);
	if (check_error(rc))
		return false;


	return true;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc;
	int priceOfCar, balance, isAvailable;


	char sql_str1[60];
	sprintf_s(sql_str1, "SELECT price FROM cars where id=%d", carid);
	rc = sqlite3_exec(db, sql_str1, callback, &priceOfCar, &zErrMsg);
	if (check_error(rc))
		return false;

	char sql_str2[60];
	sprintf_s(sql_str2, "SELECT balance FROM accounts where id=%d", buyerid);
	rc = sqlite3_exec(db, sql_str2, callback, &balance, &zErrMsg);
	if (check_error(rc))
		return false;

	if (priceOfCar < balance)
	{
		return false;
	}
	else
	{
		char sql_str3[60];
		sprintf_s(sql_str3, "SELECT available FROM cars where id=%d", carid);
		rc = sqlite3_exec(db, sql_str3, callback, &isAvailable, &zErrMsg);
		if (check_error(rc))
			return false;

		if (isAvailable == 1)
		{
			//the car can be bought
			char sql_str4[100];
			sprintf_s(sql_str4, "update cars set available = 2 where id=%d", carid);
			rc = sqlite3_exec(db, sql_str4, NULL, 0, &zErrMsg);
			if (check_error(rc))
				return false;

			char sql_str5[100];
			sprintf_s(sql_str5, "update accounts set balance = %d where id=%d", (balance-priceOfCar), buyerid);
			rc = sqlite3_exec(db, sql_str5, NULL, 0, &zErrMsg);
			if (check_error(rc))
				return false;

			return true;
		}
		else
		{
			//isAvailable = 2
			return false;
		}
	}

}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int result = 0;

	for (int i = 0; i < argc; i++)
	{
		result = result * 10 + (*argv[i] - '0');
	}

	temp = result;

	return 0;
}

int check_error(int rc)
{
	char *zErrMsg = 0;

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	return 0;
}

int main()
{
	int rc;
	sqlite3* db;
	std::stringstream s;
	char *zErrMsg = 0;

	rc = sqlite3_open("carsDealer.db", &db); // connection to the database
	if (check_error(rc))
		return 1;

	sqlite3_close(db);

	system("PAUSE");
	return 0;
}